import { ScholarlyArticle } from '../../../../lib/interfaces/ScholarlyArticle';
import { Session } from '../../../../lib/interfaces/Session';
import { Database } from '../../../../lib/lambda/middleware/withDatabase';

export async function fetchScholarlyArticle(
  database: Database,
  identifier: string,
  session?: Session,
): Promise<null | (Partial<ScholarlyArticle> & { identifier: string; creatorSessionId: string; })> {
  const accessClause = (session)
    // tslint:disable-next-line:max-line-length no-invalid-template-strings
    ? '(article.date_published IS NOT NULL OR submission.date_published IS NOT NULL OR article.creator_session=${sessionId} OR author.author=${accountId})'
    : '(article.date_published IS NOT NULL OR submission.date_published IS NOT NULL)';

  let sameAsJoin = '';
    // tslint:disable-next-line:no-invalid-template-strings
  let whereClause = ' article.identifier=${articleId}';
  let sameAsUrl;
  if (/^\d{4}\-\d{4}\-\d{4}\-\d{3}(\d|X):\d+$/.test(identifier)) {
    sameAsJoin = ' JOIN scholarly_articles_same_as same_as ON same_as.scholarly_article=article.identifier';
    // tslint:disable-next-line:no-invalid-template-strings
    whereClause = ' same_as.same_as=${sameAsUrl}';

    const [ orcid, putCode ] = identifier.split(':');
    sameAsUrl = `https://orcid.org/${orcid}/work/${putCode}`;
  }

  interface Row {
    identifier: string;
    name: string;
    description: string;
    creator_session: string;
    author?: string;
    is_part_of?: string;
    periodical_slug?: string;
    periodical_name?: string;
    date_published?: Date;
    date_published_in_periodical?: Date;
    filename?: string;
    file_content_url?: string;
  }
  const result = await database.manyOrNone<Row>(
    'SELECT'
      + ' article.identifier, article.name, article.description, article.creator_session, article.date_published'
      + ', submission.is_part_of, submission.date_published date_published_in_periodical'
      + ', periodical.identifier as periodical_slug, periodical.name as periodical_name'
      + ', author.author'
      + ', file.name as filename, file.content_url AS file_content_url'
      + ' FROM scholarly_articles article'
      + sameAsJoin
      + ' LEFT JOIN scholarly_articles_part_of submission ON article.identifier=submission.scholarly_article'
      + ' LEFT JOIN periodicals periodical ON submission.is_part_of=periodical.uuid'
      + ' LEFT JOIN scholarly_article_authors author ON author.scholarly_article=article.identifier'
      + ' LEFT JOIN scholarly_article_associated_media file ON file.identifier='
        + '('
          + 'SELECT identifier FROM scholarly_article_associated_media'
          + ' WHERE scholarly_article=article.identifier'
          + ' ORDER BY date_created DESC'
          + ' LIMIT 1'
        + ')'
      + ` WHERE ${whereClause} AND ${accessClause}`,
    {
      accountId: (session && session.account) ? session.account.identifier : undefined,
      articleId: identifier,
      sameAsUrl,
      sessionId: (session) ? session.identifier : undefined,
    },
  );

  const authors = result
    .map((row) => ({ identifier: row.author }))
    .filter((author) => typeof author.identifier !== 'undefined') as Array<{ identifier: string }>;

  if (result.length === 0) {
    return null;
  }

  let file;
  if (typeof result[0].filename === 'string' && result[0].file_content_url) {
    file = {
      contentUrl: result[0].file_content_url as string,
      license: 'https://creativecommons.org/licenses/by/4.0/' as 'https://creativecommons.org/licenses/by/4.0/',
      name: result[0].filename as string,
    };
  }

  let periodical;
  if (typeof result[0].periodical_slug === 'string') {
    periodical = {
      // Explicit type case as TypeScript doesn't detect the type here:
      identifier: result[0].periodical_slug as string,
      name: result[0].periodical_name,
    };
  }

  // We want the datePublished property to be either the article was independently published,
  // or when it was published in a periodical - whichever comes first.
  // (Both can be undefined, which is why this is so cumbersome:)
  const earliestTimestampPublished = Math.min(
    ([ result[0].date_published, result[0].date_published_in_periodical ] as any)
    // Disregard publication dates that are undefined:
    .filter((d: Date | undefined) => d instanceof Date)
    // Convert the remaining dates (if any) to timestamps that can be compared with Math.min
    .map((d: Date) => d.getTime()),
  );
  // Convert the resulting timestamp (which can be 0 if the article wasn't published anywhere yet,
  // aka `Math.min(undefined, undefined)`) to a Date object again,
  // which can in turn be converted to an ISO string as expected by the API:
  const earliestDatePublished = new Date();
  earliestDatePublished.setTime(earliestTimestampPublished);

  const article = {
    associatedMedia: (file) ? [ file ] : undefined,
    author: authors,
    creatorSessionId: result[0].creator_session,
    datePublished: (earliestTimestampPublished !== 0) ? earliestDatePublished.toISOString() : undefined,
    description: result[0].description,
    identifier: result[0].identifier,
    isPartOf: periodical,
    name: result[0].name,
  };

  return article;
}
